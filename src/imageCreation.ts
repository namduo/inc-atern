import chromium from 'chrome-aws-lambda';
import handlebars from 'handlebars';
import { registerAllHelpers } from './handlebarHelpers';
import { ITemplateParts } from './interfaces';

export const createImage: (
  template: ITemplateParts,
  saleData: any
) => Promise<string | Buffer | (string | Buffer)[]> = async (template, saleData) => {
  registerAllHelpers();
  const escapedJSON: string = JSON.stringify(saleData).replace(/\\/g, `\\\\`).replace(/'/g, `\\'`);
  const formattedTemplate: string = `
  <html>
    <head>
      <script>
        const deal = JSON.parse('${escapedJSON}');
      </script>
      ${template.css ? `<style>${template.css}</style>` : ''}
    </head>
    <body>
      ${template.html}
      ${template.js ? `<script>${template.js}</script>` : ''}
    </body>
  </html>`;
  const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(formattedTemplate, {
    noEscape: true,
  });
  const html: string = compiledTemplate(saleData);

  const browser = await chromium.puppeteer.launch({
    args: chromium.args,
    defaultViewport: { width: 1024, height: 768 },
    executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    headless: chromium.headless,
    ignoreHTTPSErrors: true,
  });
  const page = await browser.newPage();
  await page.setContent(html, { waitUntil: 'networkidle0' });
  const element = await page.$('body');
  const buffer = await element.screenshot({
    type: 'jpeg',
    omitBackground: false,
    encoding: 'base64',
    quality: 95,
  });

  await browser.close();

  return buffer;
};

export interface ITemplateParts {
  html: string;
  css?: string;
  js?: string;
}

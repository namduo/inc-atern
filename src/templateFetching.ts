import { readFileSync } from 'fs';
import { S3 } from 'aws-sdk';
import { GetObjectOutput } from 'aws-sdk/clients/s3';
import { ITemplateParts } from './interfaces';

const BUCKET: string = `se-incatern-templates-${process.env.STAGE}`;

export const fetchTemplate: (templateName: string, s3: S3) => Promise<ITemplateParts> = async (
  templateName,
  s3
) => {
  const result: ITemplateParts = { html: null };
  const useMockTemplate: boolean = process.env.IS_OFFLINE === 'true';
  const templateRelativePath: string = templateName.replace(/\-/g, '/');

  if (useMockTemplate) {
    let templateAbsolutePath: string;
    if (process.env.TEMPLATES_PATH) {
      templateAbsolutePath = `${process.env.TEMPLATES_PATH}${
        !process.env.TEMPLATES_PATH.endsWith('/') ? '/' : ''
      }${templateRelativePath}`;
    } else {
      templateAbsolutePath = `./src/templateMock/templateMock`;
    }
    console.log(`[LOCAL] Trying to use the following template files: ${templateAbsolutePath}`);
    const fileOptions: {
      encoding: string;
      flag?: string;
    } = { encoding: 'utf8', flag: 'r' };

    try {
      result.html = readFileSync(`${templateAbsolutePath}.html`, fileOptions);
    } catch (err) {
      throw new Error(
        `[LOCAL] There has been a problem fetching the file ${templateAbsolutePath}.html: (${err.message})`
      );
    }
    try {
      result.css = readFileSync(`${templateAbsolutePath}.css`, fileOptions);
    } catch (err) {
      console.info(
        `[INFO] [LOCAL] There has been a problem fetching the file ${templateAbsolutePath}.css: (${err.message})`
      );
    }
    try {
      result.js = readFileSync(`${templateAbsolutePath}.js`, fileOptions);
    } catch (err) {
      console.info(
        `[INFO] [LOCAL] There has been a problem fetching the file ${templateAbsolutePath}.js: (${err.message})`
      );
    }
  } else {
    try {
      const htmlTemplateFromS3: GetObjectOutput = await s3
        .getObject(getS3Params(templateRelativePath, 'html'))
        .promise();
      result.html = htmlTemplateFromS3.Body.toString('utf-8');
    } catch (err) {
      throw new Error(
        `There has been a problem fetching the file ${templateRelativePath}.html: (${err.message})`
      );
    }

    try {
      const cssTemplateFromS3: GetObjectOutput = await s3
        .getObject(getS3Params(templateRelativePath, 'css'))
        .promise();
      result.css = cssTemplateFromS3.Body.toString('utf-8');
    } catch (err) {
      // It is fine not to have a CSS file
      console.info(`[INFO] No CSS file (${templateRelativePath}.css)`);
    }

    try {
      const jsTemplateFromS3: GetObjectOutput = await s3
        .getObject(getS3Params(templateRelativePath, 'js'))
        .promise();
      result.js = jsTemplateFromS3.Body.toString('utf-8');
    } catch (err) {
      // It is fine not to have a JS file
      console.info(`[INFO] No JS file (${templateRelativePath}.js)`);
    }
  }

  return result;
};

const getS3Params: (
  templatePath: string,
  fileExtension: string
) => { Bucket: string; Key: string } = (templatePath, fileExtension) => {
  return {
    Bucket: BUCKET,
    Key: `${templatePath}.${fileExtension}`,
  };
};

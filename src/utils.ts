export const getDayDiff: (first: Date, second: Date) => number = (first, second) => {
  first.setUTCHours(0, 0, 0, 0);
  second.setUTCHours(0, 0, 0, 0);
  // tslint:disable-next-line: no-magic-numbers
  const MILISECONDS_PER_DAY: number = 24 * 60 * 60 * 1000;
  return Math.round(
    (Date.UTC(second.getFullYear(), second.getMonth(), second.getDate()) -
      Date.UTC(first.getFullYear(), first.getMonth(), first.getDate())) /
      MILISECONDS_PER_DAY
  );
};

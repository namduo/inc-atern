import handlebars, { HelperOptions } from 'handlebars';
import { getDayDiff } from './utils';

export const registerAllHelpers: () => void = () => {
  registerIfEqual();
  registerIfNotEqual();
  registerIfWithin();
  registerIfContains();
  registerDaysBetweenNow();
  registerDaysLeft();
  registerImageWithSize();
  registerFormatCurrency();
};

const registerIfEqual: () => void = () => {
  handlebars.registerHelper('ifEqual', function (...args) {
    const options = args.pop();
    return equals(args) ? options.fn(this) : options.inverse(this);
  });
};

const registerIfNotEqual: () => void = () => {
  handlebars.registerHelper('ifNotEqual', function (...args) {
    const options = args.pop();
    return !equals(args) ? options.fn(this) : options.inverse(this);
  });
};

const equals: (args: any[]) => boolean = args => {
  if (args.length < 2) {
    return false;
  } else {
    const first: any = args.shift();
    const reduction: any = args.reduce(
      (acc, cur) => {
        // tslint:disable-next-line: triple-equals
        return { prev: cur, result: acc.result && acc.prev == cur };
      },
      { prev: first, result: true }
    );

    return reduction.result;
  }
};

const registerIfWithin: () => void = () => {
  handlebars.registerHelper('ifWithin', function (arg, ...options) {
    if (options === undefined || options.length < 1) {
      throw Error('Incorrect number of parameters in ifWithin');
    }
    const opt = options.pop();
    if (!Number.isNaN(parseFloat(arg)) && Number.isFinite(parseFloat(arg))) {
      const min: number = opt.hash.min !== undefined ? opt.hash.min : 0;
      const max: number = opt.hash.max !== undefined ? opt.hash.max : Number.MAX_VALUE;
      const value: number = parseFloat(arg);
      if (value >= min && value <= max) {
        return opt.fn(this);
      } else {
        return opt.inverse(this);
      }
    } else {
      throw Error(`Not a numeric parameter (${arg}) in ifWithin`);
    }
  });
};

const registerIfContains: () => void = () => {
  handlebars.registerHelper('ifContains', function (arg1, arg2, ...options) {
    if (options === undefined || options.length !== 1) {
      throw Error('Incorrect number of parameters in ifContains');
    }
    if (arg1.includes(arg2)) {
      return options[0].fn(this);
    } else {
      return options[0].inverse(this);
    }
  });
};

const registerDaysBetweenNow: () => void = () => {
  handlebars.registerHelper('daysBetweenNow', (arg, ...options) => {
    if (options === undefined || options.length !== 1) {
      throw Error('Incorrect number of parameters in daysBetweenNow');
    }
    const diff: number = getDayDiff(new Date(), new Date(arg));
    if (isNaN(diff)) {
      throw Error(`Not a date parameter (${arg}) in daysBetweenNow`);
    }

    return daysLeft(diff, options[0]);
  });
};

const registerDaysLeft: () => void = () => {
  handlebars.registerHelper('daysLeft', (arg, ...options) => {
    if (options === undefined || options.length !== 1) {
      throw Error('Incorrect number of parameters in daysLeft');
    }
    if (Number.isNaN(parseFloat(arg)) || !Number.isFinite(parseFloat(arg))) {
      throw Error(`Not a numeric parameter (${arg}) in daysLeft`);
    }
    return daysLeft(parseFloat(arg), options[0]);
  });
};

const daysLeft: (arg: number, options: HelperOptions) => string = (arg, options) => {
  const format: string = options.hash.format || '%s';

  let result: string;
  if (arg === 0) {
    result = options.hash.default || 'today';
  } else if (arg === 1) {
    if (options.hash.singular_override) {
      result = options.hash.singular_override;
    } else if (options.hash.singular) {
      result = format.replace(/%[a-z]/, `${arg} ${options.hash.singular}`);
    } else {
      result = format.replace(/%[a-z]/, `${arg}`);
    }
  } else {
    if (options.hash.plural) {
      result = format.replace(/%[a-z]/, `${arg} ${options.hash.plural}`);
    } else {
      result = format.replace(/%[a-z]/, `${arg}`);
    }
  }

  return handlebars.escapeExpression(result);
};

const registerImageWithSize: () => void = () => {
  handlebars.registerHelper('imageWithSize', (arg, ...options) => {
    if (options === undefined || options.length !== 1) {
      throw Error('Incorrect number of parameters in imageWithSize');
    }
    if (!options[0].hash.w || !options[0].hash.h) {
      throw Error('Both width and height are mandatory in imageWithSize');
    }

    return new handlebars.SafeString(
      arg.replace('$width', options[0].hash.w).replace('$height', options[0].hash.h)
    );
  });
};

const registerFormatCurrency: () => void = () => {
  handlebars.registerHelper('formatCurrency', (...args) => {
    const options = args.pop();
    const arg: any = args.length > 0 ? args.shift() : 0;
    if (!Number.isNaN(parseFloat(arg)) && Number.isFinite(parseFloat(arg))) {
      const value: number = parseFloat(arg);
      const symbol: string = options.hash.symbol || '£';
      const decimals: number =
        options.hash.showCents != null && !Boolean(options.hash.showCents) ? 0 : 2;

      return handlebars.escapeExpression(
        `${symbol}${value.toFixed(decimals).replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',')}`
      );
    } else {
      throw Error(`Not a numeric parameter (${arg}) in formatCurrency`);
    }
  });
};

import * as fetch from 'node-fetch';
import { getDayDiff } from './utils';

const territoryInformation: {
  [territory: string]: { affiliate: string; currency: string };
} = {
  UK: { affiliate: 'es', currency: 'GBP' },
  SE: { affiliate: 'sv', currency: 'SEK' },
  SE_TEMP: { affiliate: 'applitools-affiliate-main', currency: 'GBP' },
  DE: { affiliate: 'de', currency: 'EUR' },
  'GUARDIAN - UK': { affiliate: 'guardianstaff', currency: 'GBP' },
  US: { affiliate: 'us', currency: 'USD' },
  DK: { affiliate: 'dk', currency: 'DKK' },
  NO: { affiliate: 'no', currency: 'NOK' },
  CH: { affiliate: 'ch', currency: 'CHF' },
  IT: { affiliate: 'it', currency: 'EUR' },
  NL: { affiliate: 'nl', currency: 'EUR' },
  ES: { affiliate: 'esp', currency: 'EUR' },
  BE: { affiliate: 'be', currency: 'EUR' },
  FR: { affiliate: 'fr', currency: 'EUR' },
  SG: { affiliate: 'sg', currency: 'SGD' },
  HK: { affiliate: 'hk', currency: 'HKD' },
  PH: { affiliate: 'ph', currency: 'PHP' },
  ID: { affiliate: 'id', currency: 'IDR' },
  MY: { affiliate: 'my', currency: 'MYR' },
  CZ: { affiliate: 'cz', currency: 'CZK' },
  HU: { affiliate: 'hu', currency: 'HUF' },
  SK: { affiliate: 'sk', currency: 'EUR' },
  'TB-BE_FR': { affiliate: 'travelbirdbefr', currency: 'EUR' },
  'TB-BE_NL': { affiliate: 'travelbirdbe', currency: 'EUR' },
  'TB-NL': { affiliate: 'travelbirdnl', currency: 'EUR' },
  IE: { affiliate: 'pigsback', currency: 'EUR' },
};

const SPARROW_URL: string = 'https://prod-cf.sparrow.escapes.tech/graphql';

const PERCENTAGE: number = 100;

export const getSaleInfo: (territory: string, saleId: string) => Promise<JSON> = async (
  territory,
  saleId
) => {
  const formattedTerritory: string = territory.toUpperCase();
  const information: { affiliate: string; currency: string } =
    territoryInformation[formattedTerritory];
  if (!information) {
    throw Error(`Incorrect territory name (${territory})`);
  }
  const affiliate: string = information.affiliate;
  const currency: string = information.currency;
  const query = {
    query: `
      query Sale {
        sale(affiliate:"${affiliate}",currency:"${currency}",saleId:"${saleId}") {
            id,
            type,
            dates {
                start,
                end
            },
            tags,
            photos {
                url,
                urlWithSize,
                caption
            }
            prices {
                currency {
                    currencyCode
                }
                discount,
                discountTooltip,
                depositFromPrice {
                    forDisplay,
                    unit,
                    unitPerPerson
                }
                leadRate {
                    forDisplay,
                    unit,
                    unitPerPerson
                }
                leadRateLabel,
                leadRateTooltip,
                leadRateUnitLabel,
                rackRate {
                    forDisplay,
                    unit,
                    unitPerPerson
                },
                maxNumberOfAdults,
                numberOfHotelNights,
                pricingModelForDisplay,
                pricingRules {
                    showDiscount,
                    showPrices,
                    showRackRate
                }
            }
            attributes {
                isMysterious,
                isDynamicPackage,
                isSmartStay,
                isEditorsPick,
                isExclusive,
                isDepositSale,
                isCurrent,
                isConnected,
                isTimeLimited
            },
            links {
                sale,
                priceComparison,
                tripAdvisor
            },
            promotion {
                label,
                description,
                hexCode,
                tagName,
                iconCode
            },
            labels {
                hexCode,
                iconCode,
                labelKey,
                summary
            },
            travel {
                hasFlightsIncluded,
                hasFlightsAvailable,
                travelType
            },
            location {
                city {
                    id,
                    name
                },
                country {
                    id,
                    name
                },
                continent {
                    id,
                    name
                },
                division {
                    id,
                    name
                },
                latitude,
                longitude
            }
            editorial {
                title,
                destinationName,
                reasonToLove,
                roomDescription,
                dealIncludes,
                weLike,
                mainParagraph,
                hotelDetails,
                secondOpinion,
                travelDetails,
                summary
            },
            bookingStats {
                timesBooked,
                currentVisitors
            }
        }
      }`,
  };

  return fetch(SPARROW_URL, {
    method: 'POST',
    body: JSON.stringify(query),
    headers: { 'Content-Type': 'application/json' },
  })
    .then(res => {
      if (!res || !res.ok) {
        throw new Error(`No information available for that sale id (${saleId})`);
      }

      return res.json();
    })
    .then(json => {
      if (!json) {
        throw new Error(`No information available for that sale id (${saleId})`);
      }
      if (json.errors) {
        throw new Error(json.errors[0].message);
      }
      if (!json.data || !json.data.sale) {
        throw new Error(`Invalid information for that sale id (${saleId})`);
      }

      return json.data.sale;
    })
    .then(saleData => {
      return {
        ...saleData,
        dealId: saleData.id,
        title: saleData.editorial?.title,
        image: {
          large: saleData.photos[0]?.url,
          medium: saleData.photos[0]?.urlWithSize
            ?.replace('$width', '330')
            ?.replace('$height', '230'),
          small: saleData.photos[1]?.url,
          sidebar: saleData.photos[2]?.url,
        },
        originalPrice: saleData.prices?.rackRate?.unit,
        currentPrice: saleData.prices?.leadRate?.unit,
        savingsRate:
          saleData.prices?.rackRate?.unit && saleData.prices?.leadRate?.unit
            ? Math.round(
                ((saleData.prices.rackRate.unit - saleData.prices.leadRate.unit) * PERCENTAGE) /
                  saleData.prices.rackRate.unit
              )
            : 0,
        startDate: saleData.dates?.start,
        endDate: saleData.dates?.end,
        url: saleData.links?.sale,
        info: {
          tags: saleData.tags,
          product_type: saleData.type?.toUpperCase(),
          destination_name: saleData.editorial?.destinationName,
          daysLeft: saleData.dates?.end
            ? getDayDiff(new Date(), new Date(saleData.dates.end))
            : Number.MAX_VALUE,
          discount: saleData.prices?.discount ?? 0,
          price_description: saleData.prices?.leadRateLabel,
          travel:
            saleData.travel?.travelType !== '' ? saleData.travel.travelType.toUpperCase() : 'NONE',
          reasonToLove: saleData.editorial?.reasonToLove,
          destination: [
            saleData.type?.toUpperCase(),
            null,
            saleData.location?.country?.name,
            saleData.location?.division?.name,
            saleData.location?.city?.name,
          ],
          continent: saleData.location?.continent?.name,
          country: saleData.location?.country?.name,
          division: saleData.location?.division?.name,
          city: saleData.location?.city?.name,
        },
        l1Category: saleData.tags[0],
        l2Category: saleData.tags[1],
        l3Category: saleData.tags[2],
        // tslint:disable-next-line: no-magic-numbers
        l4Category: saleData.tags[3],
        territory: formattedTerritory,
      };
    });
};

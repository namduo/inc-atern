import { createImage } from '../imageCreation';
import { ITemplateParts } from '../interfaces';
// skipped due to error at jenkins: Error: Failed to launch the browser process! spawn /tmp/chromium ENOENT
test.skip('correct greeting is generated', async () => {
  const saleCardTemplate: ITemplateParts = { html: '<div>{{id}} here will be a sale card</div>' };
  const MIN_IMAGE_LENGTH: number = 1000;
  expect(await (await createImage(saleCardTemplate, mockedResponse)).length).toBeGreaterThan(
    MIN_IMAGE_LENGTH
  );
});

const mockedResponse = {
  id: 'A14794',
  editorial: {
    title: 'Boutique Lisbon Heritage hotel with Art Deco interiors - Fully refundable',
    destinationName: 'Hotel Britania, Portugal',
    summary:
      'Stunning Art Deco style at a boutique Lisbon Heritage hotel near the Avenida da Liberdade, including breakfast, sightseeing perks, welcome wine and more. Fully refundable - book with confidence',
  },
  photos: [
    {
      url:
        'https://secretescapes-web.imgix.net/hotels/962/93f9a844_8114_4399_a6f5_532d031f47a9.jpg?auto=format,compress',
    },
  ],
  prices: {
    leadRate: {
      forDisplay: '£84',
    },
    discount: 75,
  },
  attributes: {
    isEditorsPick: false,
  },
};

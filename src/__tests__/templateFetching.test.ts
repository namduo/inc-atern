import { fetchTemplate } from '../templateFetching';
import { readFileSync } from 'fs';
import AWS from 'aws-sdk';
import { ITemplateParts } from '../interfaces';

const getObjectWithContents: (params: any) => any = params => {
  return {
    promise: () => {
      return { Body: Buffer.from(`template-from-aws ${params.Key}`) };
    },
  };
};

test('when in offline mode, the content of mock file is fetched', async () => {
  process.env.IS_OFFLINE = 'true';

  const fileOptions: {
    encoding: string;
    flag?: string;
  } = { encoding: 'utf8', flag: 'r' };

  const htmlData = readFileSync('./src/templateMock/templateMock.html', fileOptions);
  const cssData = readFileSync('./src/templateMock/templateMock.css', fileOptions);
  const jsData = readFileSync('./src/templateMock/templateMock.js', fileOptions);

  const result: ITemplateParts = await fetchTemplate('templateName', null);

  expect(result.html).toBe(htmlData);
  expect(result.css).toBe(cssData);
  expect(result.js).toBe(jsData);
});

test('when online mode, the template from S3 is returned', async () => {
  process.env.IS_OFFLINE = 'false';
  const s3 = new AWS.S3({ region: 'us-west-2' }) as any;
  s3.getObject = jest.fn((_params, _cb) => getObjectWithContents(_params));

  const result: ITemplateParts = await fetchTemplate('folder-templateName', s3);
  expect(result.html).toBe('template-from-aws folder/templateName.html');
  expect(result.css).toBe('template-from-aws folder/templateName.css');
  expect(result.js).toBe('template-from-aws folder/templateName.js');
  expect(s3.getObject).toHaveBeenCalledWith(
    expect.objectContaining({
      Key: 'folder/templateName.html',
    })
  );
  expect(s3.getObject).toHaveBeenCalledWith(
    expect.objectContaining({
      Key: 'folder/templateName.css',
    })
  );
  expect(s3.getObject).toHaveBeenCalledWith(
    expect.objectContaining({
      Key: 'folder/templateName.js',
    })
  );
});

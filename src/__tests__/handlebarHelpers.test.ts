import handlebars from 'handlebars';
import { registerAllHelpers } from '../handlebarHelpers';

const TODAY: Date = new Date();

const testData: any = {
  num: 10,
  str: 'Abc',
  arrNum: [0, 1, 2],
  arrStr: ['A', 'B', 'C'],
  today: TODAY,
  tomorrow: new Date(TODAY).setDate(TODAY.getDate() + 1),
  inTwoDays: new Date(TODAY).setDate(TODAY.getDate() + 2),
  yesterday: new Date(TODAY).setDate(TODAY.getDate() - 1),
  strToday: TODAY.toISOString(),
  urlWithSize: 'https://test/image?w=$width&h=$height&fit=crop',
  price1: 1525,
  price2: 1525.99,
  price3: 1525.996,
};

beforeAll(() => {
  registerAllHelpers();
});

describe('ifEqual', () => {
  it.each`
    params                    | expected
    ${''}                     | ${'Negative'}
    ${'num'}                  | ${'Negative'}
    ${'num 10'}               | ${'Positive'}
    ${'num "10"'}             | ${'Positive'}
    ${'num 20'}               | ${'Negative'}
    ${'num "A"'}              | ${'Negative'}
    ${'num 10 "10" 10'}       | ${'Positive'}
    ${'num 10 "10" 10 20 10'} | ${'Negative'}
  `('works properly ($params)', ({ params, expected }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{#ifEqual ${params}}}Positive{{else}}Negative{{/ifEqual}}`
    );
    expect(compiledTemplate(testData)).toBe(expected);
  });
});

describe('ifNotEqual', () => {
  it.each`
    params                    | expected
    ${''}                     | ${'Positive'}
    ${'num'}                  | ${'Positive'}
    ${'num 10'}               | ${'Negative'}
    ${'num "10"'}             | ${'Negative'}
    ${'num 20'}               | ${'Positive'}
    ${'num "A"'}              | ${'Positive'}
    ${'num 10 "10" 10'}       | ${'Negative'}
    ${'num 10 "10" 10 20 10'} | ${'Positive'}
  `('works properly ($params)', ({ params, expected }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{#ifNotEqual ${params}}}Positive{{else}}Negative{{/ifNotEqual}}`
    );
    expect(compiledTemplate(testData)).toBe(expected);
  });
});

describe('ifWithin', () => {
  it('incorrect number of parameters', () => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{#ifWithin}}Test{{/ifWithin}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/incorrect.*ifWithin/i);
  });

  it('non-numeric parameter', () => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{#ifWithin "Abc" min=10 max=20}}Test{{/ifWithin}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/numeric.*parameter.*ifWithin/i);
  });

  it.each`
    params                 | expected
    ${'num'}               | ${'Positive'}
    ${'-5'}                | ${'Negative'}
    ${'num min=0 max=20'}  | ${'Positive'}
    ${'num min=0'}         | ${'Positive'}
    ${'num max=20'}        | ${'Positive'}
    ${'num min=10 max=20'} | ${'Positive'}
    ${'num min=0 max=10'}  | ${'Positive'}
    ${'num min=10'}        | ${'Positive'}
    ${'num max=10'}        | ${'Positive'}
    ${'10 min=0 max=20'}   | ${'Positive'}
    ${'"10" min=0 max=20'} | ${'Positive'}
    ${'num min=15 max=20'} | ${'Negative'}
    ${'num min=0 max=5'}   | ${'Negative'}
    ${'num min=15'}        | ${'Negative'}
    ${'num max=5'}         | ${'Negative'}
  `('works properly ($params)', ({ params, expected }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{#ifWithin ${params}}}Positive{{else}}Negative{{/ifWithin}}`
    );
    expect(compiledTemplate(testData)).toBe(expected);
  });
});

describe('ifContains', () => {
  it.each`
    params
    ${'str'}
    ${'str "a" "b"'}
  `('incorrect number of parameters ($params)', ({ params }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{#ifContains ${params}}}Test{{/ifContains}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/incorrect.*ifContains/i);
  });

  it.each`
    params           | expected
    ${'str "A"'}     | ${'Positive'}
    ${'str "b"'}     | ${'Positive'}
    ${'str "Ab"'}    | ${'Positive'}
    ${'arrNum 1'}    | ${'Positive'}
    ${'arrStr "A"'}  | ${'Positive'}
    ${'str "a"'}     | ${'Negative'}
    ${'str "x"'}     | ${'Negative'}
    ${'arrNum 5'}    | ${'Negative'}
    ${'arrStr "a"'}  | ${'Negative'}
    ${'arrStr "AB"'} | ${'Negative'}
    ${'arrStr "X"'}  | ${'Negative'}
  `('works properly ($params)', ({ params, expected }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{#ifContains ${params}}}Positive{{else}}Negative{{/ifContains}}`
    );
    expect(compiledTemplate(testData)).toBe(expected);
  });
});

describe('daysBetweenNow', () => {
  it.each`
    params
    ${''}
    ${'today tomorrow'}
  `('incorrect number of parameters ($params)', ({ params }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{daysBetweenNow ${params}}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/incorrect.*daysBetweenNow/i);
  });

  it.each`
    params
    ${'str'}
    ${'arrStr'}
  `('invalid date parameter ($params)', ({ params }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{daysBetweenNow ${params}}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/date.*daysBetweenNow/i);
  });

  it.each`
    params                                                                                                  | expected
    ${'today'}                                                                                              | ${'Ends today'}
    ${'today format="in %s"'}                                                                               | ${'Ends today'}
    ${'today format="in %s" singular="day"'}                                                                | ${'Ends today'}
    ${'today format="in %s" singular="day" plural="days"'}                                                  | ${'Ends today'}
    ${'today format="in %s" singular="day" plural="days" singular_override="tomorrow"'}                     | ${'Ends today'}
    ${'today format="in %s" default="the same day"'}                                                        | ${'Ends the same day'}
    ${'tomorrow'}                                                                                           | ${'Ends 1'}
    ${'tomorrow singular_override="tomorrow"'}                                                              | ${'Ends tomorrow'}
    ${'tomorrow format="in %s"'}                                                                            | ${'Ends in 1'}
    ${'tomorrow format="in %s" singular_override="tomorrow"'}                                               | ${'Ends tomorrow'}
    ${'tomorrow format="in %s" default="today"'}                                                            | ${'Ends in 1'}
    ${'tomorrow format="in %s" default="today" singular_override="tomorrow"'}                               | ${'Ends tomorrow'}
    ${'tomorrow format="in %s" default="today" singular="day"'}                                             | ${'Ends in 1 day'}
    ${'tomorrow format="in %s" default="today" singular="day" singular_override="tomorrow"'}                | ${'Ends tomorrow'}
    ${'tomorrow format="in %s" default="today" plural="days"'}                                              | ${'Ends in 1'}
    ${'tomorrow format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'}  | ${'Ends tomorrow'}
    ${'inTwoDays'}                                                                                          | ${'Ends 2'}
    ${'inTwoDays format="in %s"'}                                                                           | ${'Ends in 2'}
    ${'inTwoDays format="in %s" default="today"'}                                                           | ${'Ends in 2'}
    ${'inTwoDays format="in %s" default="today" singular="day"'}                                            | ${'Ends in 2'}
    ${'inTwoDays format="in %s" default="today" singular="day" singular_override="tomorrow"'}               | ${'Ends in 2'}
    ${'inTwoDays format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'} | ${'Ends in 2 days'}
    ${'yesterday'}                                                                                          | ${'Ends -1'}
    ${'yesterday format="in %s"'}                                                                           | ${'Ends in -1'}
    ${'yesterday format="in %s" default="today"'}                                                           | ${'Ends in -1'}
    ${'yesterday format="in %s" default="today" singular="day"'}                                            | ${'Ends in -1'}
    ${'yesterday format="in %s" default="today" singular="day" singular_override="tomorrow"'}               | ${'Ends in -1'}
    ${'yesterday format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'} | ${'Ends in -1 days'}
    ${'strToday format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'}  | ${'Ends today'}
  `('works properly ($params)', ({ params, expected }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `Ends {{daysBetweenNow ${params}}}`
    );
    expect(compiledTemplate(testData)).toBe(expected);
  });
});

describe('daysLeft', () => {
  it.each`
    params
    ${''}
    ${'today tomorrow'}
  `('incorrect number of parameters ($params)', ({ params }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{daysLeft ${params}}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/incorrect.*daysLeft/i);
  });

  it('non-numeric parameter', () => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{daysLeft "Abc" format="in %s" singular="day" plural="days" singular_override="tomorrow"}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/numeric.*parameter.*daysLeft/i);
  });

  it.each`
    params                                                                                            | expected
    ${'0'}                                                                                            | ${'Ends today'}
    ${'0 format="in %s"'}                                                                             | ${'Ends today'}
    ${'0 format="in %s" singular="day"'}                                                              | ${'Ends today'}
    ${'0 format="in %s" singular="day" plural="days"'}                                                | ${'Ends today'}
    ${'0 format="in %s" singular="day" plural="days" singular_override="tomorrow"'}                   | ${'Ends today'}
    ${'0 format="in %s" default="the same day"'}                                                      | ${'Ends the same day'}
    ${'1'}                                                                                            | ${'Ends 1'}
    ${'1 singular_override="tomorrow"'}                                                               | ${'Ends tomorrow'}
    ${'1 format="in %s"'}                                                                             | ${'Ends in 1'}
    ${'1 format="in %s" singular_override="tomorrow"'}                                                | ${'Ends tomorrow'}
    ${'1 format="in %s" default="today"'}                                                             | ${'Ends in 1'}
    ${'1 format="in %s" default="today" singular_override="tomorrow"'}                                | ${'Ends tomorrow'}
    ${'1 format="in %s" default="today" singular="day"'}                                              | ${'Ends in 1 day'}
    ${'1 format="in %s" default="today" singular="day" singular_override="tomorrow"'}                 | ${'Ends tomorrow'}
    ${'1 format="in %s" default="today" plural="days"'}                                               | ${'Ends in 1'}
    ${'1 format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'}   | ${'Ends tomorrow'}
    ${'2'}                                                                                            | ${'Ends 2'}
    ${'2 format="in %s"'}                                                                             | ${'Ends in 2'}
    ${'2 format="in %s" default="today"'}                                                             | ${'Ends in 2'}
    ${'2 format="in %s" default="today" singular="day"'}                                              | ${'Ends in 2'}
    ${'2 format="in %s" default="today" singular="day" singular_override="tomorrow"'}                 | ${'Ends in 2'}
    ${'2 format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'}   | ${'Ends in 2 days'}
    ${'-1'}                                                                                           | ${'Ends -1'}
    ${'-1 format="in %s"'}                                                                            | ${'Ends in -1'}
    ${'-1 format="in %s" default="today"'}                                                            | ${'Ends in -1'}
    ${'-1 format="in %s" default="today" singular="day"'}                                             | ${'Ends in -1'}
    ${'-1 format="in %s" default="today" singular="day" singular_override="tomorrow"'}                | ${'Ends in -1'}
    ${'-1 format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'}  | ${'Ends in -1 days'}
    ${'"0" format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'} | ${'Ends today'}
    ${'"1" format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'} | ${'Ends tomorrow'}
    ${'"2" format="in %s" default="today" singular="day" plural="days" singular_override="tomorrow"'} | ${'Ends in 2 days'}
  `('works properly ($params)', ({ params, expected }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `Ends {{daysLeft ${params}}}`
    );
    expect(compiledTemplate(testData)).toBe(expected);
  });
});

describe('imageWithSize', () => {
  it.each`
    params
    ${''}
    ${'urlWithSize "https://test"'}
  `('incorrect number of parameters ($params)', ({ params }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{imageWithSize ${params}}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/incorrect.*imageWithSize/i);
  });

  it.each`
    params
    ${'urlWithSize w=100'}
    ${'urlWithSize h=100'}
  `('incorrect number of arguments ($params)', ({ params }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{imageWithSize ${params}}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/mandatory.*imageWithSize/i);
  });

  it('works properly', () => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{imageWithSize urlWithSize w=100 h=50}}`
    );
    expect(compiledTemplate(testData)).toBe('https://test/image?w=100&h=50&fit=crop');
  });
});

describe('formatCurrency', () => {
  it('non-numeric parameter', () => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{formatCurrency "Abc" symbol="£" showCents=true}}`
    );
    expect(() => {
      compiledTemplate(testData);
    }).toThrowError(/numeric.*parameter.*formatCurrency/i);
  });

  it.each`
    params                                   | expected
    ${''}                                    | ${'£0.00'}
    ${'price1'}                              | ${'£1,525.00'}
    ${'price1 price2'}                       | ${'£1,525.00'}
    ${'price1 symbol="$"'}                   | ${'$1,525.00'}
    ${'price1 showCents=true'}               | ${'£1,525.00'}
    ${'price1 showCents=false'}              | ${'£1,525'}
    ${'price1 symbol="$" showCents=true'}    | ${'$1,525.00'}
    ${'price1 symbol="$" showCents=false'}   | ${'$1,525'}
    ${'price1 symbol="$" showCents="false"'} | ${'$1,525.00'}
    ${'price2'}                              | ${'£1,525.99'}
    ${'price2 symbol="$" showCents=false'}   | ${'$1,526'}
    ${'price3'}                              | ${'£1,526.00'}
    ${'price3 symbol="$" showCents=false'}   | ${'$1,526'}
  `('works properly ($params)', ({ params, expected }) => {
    const compiledTemplate: HandlebarsTemplateDelegate = handlebars.compile(
      `{{formatCurrency ${params}}}`
    );
    expect(compiledTemplate(testData)).toBe(expected);
  });
});

// {{formatCurrency currentPrice symbol="£" showCents=false}}

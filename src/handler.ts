import { APIGatewayProxyHandler } from 'aws-lambda';
import { createImage } from './imageCreation';
import { fetchTemplate } from './templateFetching';
import AWS from 'aws-sdk';
import { getSaleInfo } from './saleInfo';
import { ITemplateParts } from './interfaces';

const hrtimeTupleToMilis: (hrtime: [number, number]) => number = hrtime => {
  // tslint:disable-next-line: no-magic-numbers
  return Math.round(hrtime[0] * 1000 + hrtime[1] / 1000000);
};

export const generateimage: APIGatewayProxyHandler = async (event, _context) => {
  const parameters: string = `${event.pathParameters.territoryname}/${event.pathParameters.saleid}/${event.pathParameters.templateid}`;

  let template: ITemplateParts;
  let start: [number, number] = process.hrtime();
  try {
    template = await fetchTemplate(event.pathParameters.templateid, new AWS.S3());
  } catch (err) {
    console.error(
      `[ERROR] There has been a problem fetching the template\nParameters: ${parameters}\nError: ${err.message}`
    );

    return {
      statusCode: 404,
      body: `There has been a problem fetching the template: (${err.message}).`,
    };
  }
  const templatesFetched: [number, number] = process.hrtime(start);
  start = process.hrtime();

  let saleInfo: JSON;
  try {
    saleInfo = await getSaleInfo(event.pathParameters.territoryname, event.pathParameters.saleid);
  } catch (err) {
    console.error(
      `[ERROR] There has been a problem getting the sale information\nParameters: ${parameters}\nError: ${err.message}`
    );

    return {
      statusCode: 404,
      body: `There has been a problem getting the sale information: (${err.message}).`,
    };
  }
  const saleInfoFetched: [number, number] = process.hrtime(start);
  start = process.hrtime();

  let imageAsBase64: string;
  try {
    imageAsBase64 = (await createImage(template, saleInfo)) as string;
  } catch (err) {
    console.error(
      `[ERROR] There has been a problem creating the image\nParameters: ${parameters}\nError: ${err.message}`
    );

    return {
      statusCode: 500,
      body: `There has been a problem creating the image: (${err.message}).`,
    };
  }
  const cardCreated: [number, number] = process.hrtime(start);
  // tslint:disable: no-magic-numbers
  console.info(
    `Times: template ${hrtimeTupleToMilis(templatesFetched)} saleInfo ${hrtimeTupleToMilis(
      saleInfoFetched
    )} image ${hrtimeTupleToMilis(cardCreated)}`
  );
  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'image/jpeg',
      'Content-Disposition': `filename="${event.pathParameters.saleid}${event.pathParameters.territoryname}.jpeg"`,
    },
    body: imageAsBase64,
    isBase64Encoded: true,
  };
};

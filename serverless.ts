import type { Serverless } from 'serverless/aws';

const serverlessConfiguration: Serverless = {
  service: {
    name: 'incatern-card-creator',
    // app and org for use with dashboard.serverless.com
    // app: your-app-name,
    // org: your-org-name,
  },
  frameworkVersion: '>=2.4.0',
  custom: {
    webpack: {
      webpackConfig: './webpack.config.js',
      includeModules: {
        forceExclude: ['aws-sdk'],
      },
    },
    customDomain: {
      domainName: 'incatern-${opt:stage}.escapes.tech',
      certificateName: 'incatern-${opt:stage}.escapes.tech',
      basePath: '',
      endpointType: 'regional',
      stage: '${opt:stage}',
      createRoute53Record: true,
    },
  },
  // Add the serverless-webpack plugin
  plugins: ['serverless-webpack', 'serverless-offline', 'serverless-domain-manager'],
  provider: {
    name: 'aws',
    runtime: 'nodejs12.x',
    region: 'eu-west-1',
    stage: '${opt:stage}',
    memorySize: 2048,
    deploymentBucket: {
      name: 'se-incatern-serverless-${opt:stage}',
    },
    apiGateway: {
      binaryMediaTypes: ['*/*'],
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      STAGE: '${opt:stage}',
    },
    iamRoleStatements: [
      {
        Effect: 'Allow',
        Action: 's3:GetObject',
        Resource: {
          'Fn::Join': ['', ['arn:aws:s3:::se-incatern-templates-${opt:stage}', '/*']],
        },
      },
    ],
  },
  functions: {
    generateimage: {
      handler: 'src/handler.generateimage',
      tags: { release: '${file(package.json):version}' },
      timeout: 15,
      events: [
        {
          http: {
            method: 'get',
            path: 'generateimage/{territoryname}/{saleid}/{templateid}',
            request: {
              parameters: {
                paths: { saleid: true, territoryname: true, templateid: true },
              },
            },
          },
        },
      ],
    },
  },
};

module.exports = serverlessConfiguration;

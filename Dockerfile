FROM node:10-alpine

RUN apk update && apk add curl bash git openssh

ENV YARN_VERSION 1.9.4
RUN apk add --no-cache --virtual .build-deps-yarn curl tar \
    && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" 

RUN mkdir -p /opt

RUN tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/

RUN ln -sf /opt/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn \
    && ln -sf /opt/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg 

RUN rm yarn-v$YARN_VERSION.tar.gz \
    && apk del .build-deps-yarn

# Context

Repo contains of ecosystem of lambdas that fulfill request done by Marketing team.
They requested a service that generates sale card images based on [handlebars templates](https://handlebarsjs.com/guide/#what-is-handlebars) located in [repository accessible for Marketing team](https://github.com/secretescapes/incatern-templates) and [Sparrow's](https://github.com/secretescapes/Sparrow) response.

Entire orchestration (when tool is invoked, how marketing emails are sent) is done by Salesforce tool and out of scope of this repo.

![](mkt-use-case.png)

# Usage

`yarn start` runs service in localhost (offline) mode. It exposes the endpoint you can reach via HTTP `http://localhost:3000/dev/generateimage/UK/A14794/whatever` but uses [local, fixed template](https://github.com/secretescapes/incatern-card-creator/blob/master/src/templateMock/templateMock.html) files instead of those in S3 bucket. It does not matter what template name you provide in URL.

When in offline mode, an absolute path for the templates to be used can be specified using an environment variable with the name TEMPLATES_PATH. That variable can be set in the environment or provided in the yarn command: `TEMPLATES_PATH=/absolute/path/to/my/templates yarn start`. If you go that way, the path from env variable will be concatenated with path provided as URL param and this will be the source where app will pick files from. 

Lambda version is deployed Incatern's subbacount - [one for staging and one for production](https://github.com/secretescapes/infrastructure/wiki/AWS).

Staging lambda url: `https://umcy2bg3h4.execute-api.eu-west-1.amazonaws.com/staging/generateimage/DE/A14799/test` is mapped via Route53 to `https://incatern-staging.escapes.tech/generateimage/DE/A14799/test`

Production url: `https://tyy0fovu85.execute-api.eu-west-1.amazonaws.com/production/generateimage/DE/A14799/test1` is mapped to `https://incatern-production.escapes.tech/generateimage/DE/A14799/test`

# Deploying new version

Deployment is done via [jenkins job](https://jenkins.secretescapes.com/view/All/job/IncaTern/job/Deploy%20incatern-card-creator/)

# Requirements from MKT

Most of discussion is gathered in [google doc](https://docs.google.com/document/d/172ZSrAgUpGtzghgXpTUeLqPtxoS5Q-TsDPSBbBvr83U/edit)

- we need to generate JPEG that has weight capped at 200kb
- processing of 5000 sales has to be done in mas 1h
- no transparend background needed
- images might have different sizes - should be defined by template

# Troubleshooting

1. If you get messages that offline mode after `yarn start` invoked is not available, then you might need to update your serverless application `sudo npm update serverless -g`
2. In case of `Error: Failed to launch the browser process!` try to stop serverless app, remove `/tmp/chromium` file and start serverless app again
3. If second invocation of offline app throws `TypeError: Cannot read property 'defineService' of undefined`, play a bit with version of serverless-offline lib. We had such issues and that's why library is fixed to version 6.6.0 in package.json
4. Mac OS cannot read chromium that is compiled for lambda. On Macs you need to point to your binary of chrome in order to make offline app working. It can be done by pointing [executablePath](https://github.com/secretescapes/incatern-card-creator/blob/master/src/imageCreation.ts#L33) to browser's binary at your system, eg: `executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'` 

module.exports = {
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.ts?$',
  testURL: 'http://localhost',
  moduleFileExtensions: ['ts', 'js'],
};
